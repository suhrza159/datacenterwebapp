<?php
session_start();
include('includes/header-1.php');
include('includes/navbar.php');
?>
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h2 class="m-0 font-weight-bold text-primary">Weekly
    
    </h2>
    
  </div>

  <div class="card-body">

    <div class="table-responsive">

      <?php
      include('connect.php');

      $sql = "SELECT * FROM WEEKLY";
      $query = mysqli_query($con, $sql);
      ?>


<table class="table table-striped table-bordored mydatatable" style="width: 100%">
            <thead>
                <tr>
                    <th colspan="1" rowspan="2">WEEK</th>
                   
                    
                </tr>
                <tr>
                    <th>CUST</th>
                    <th>MODEL</th>
                    <th>MACHINE_ID</th>
                    <th>STATION_DESCRP</th>
                    <th>GOOD</th>
                    <th>NG</th>
                    <th>FYR</th>
                    
                </tr>
            </thead>
            <tbody>
            <?php
            if (mysqli_num_rows($query) > 0) {
              while ($row = mysqli_fetch_assoc($query)) {
                ?>
                <tr>
                    <td><?php echo $row['WEEK']; ?></td>
                    <td><?php echo $row['CUST']; ?></td>
                    <td><?php echo $row['MODEL']; ?></td>
                    <td><?php echo $row['MACHINE_ID']; ?></td>
                    <td><?php echo $row['STATION_DESCRP']; ?></td>
                    <td><?php echo $row['GOOD_TOT_QTY']; ?></td>
                    <td><?php echo $row['NG_TOT_QTY']; ?></td>
                    <td><?php echo $row['FYR']; ?></td>
                </tr>
                <?php
              }
            } else {
              echo "No Record Found";
            }
            ?>
               
            </tbody>
            <tfoot>
                <tr>
                    <th>WEEK</th>
                    <th>CUST</th>
                    <th>MODEL</th>
                    <th>MACHINE_ID</th>
                    <th>STATION_DESCRP</th>
                    <th>GOOD</th>
                    <th>NG</th>
                    <th>FYR</th>
                </tr>
            </tfoot>
        </table>


    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/script-1.php');
include('includes/footer.php');
?>