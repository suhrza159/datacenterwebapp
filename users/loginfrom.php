<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="/css/bootstrap.css" />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
    />
    <title>Data Center | Cal-Comp Electronics Thailand</title>
    <link
      rel="icon"
      type="image/png"
      href="/img/NKG_LOGO_20160629_CS5_2.png"
    />
    <link rel="stylesheet" href="/css/login.css">
  </head>
  <body>
    <section class="login-block">
      <div class="container">
    <div class="row">
      <div class="col-md-4 login-sec">
          <h2 class="text-center">Login</h2>
          <form class="login-form" method="POST" action="login.php">
    <div class="form-group">
      <label for="exampleInputEmail1" class="text-uppercase">Username</label>
      <input type="text" class="form-control" placeholder="" id="Username" name="Username">
      
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1" class="text-uppercase">Password</label>
      <input type="password" class="form-control" placeholder="" id="Password" name="Password">
    </div>
    
    
      <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input">
        <small>Remember Me</small>
      </label>
      <button type="submit" class="btn btn-login float-right">Login</button>
    </div>
    
  </form>
  <div class="copy-text">Created with <i class="fa fa-heart"></i> by MIS Department </div>
      </div>
      <div class="col-md-8 banner-sec">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                   <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
              <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="/img/plaining.jpg" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <div class="banner-text">
              <h2>Vision and Mission</h2>
              <p>Photo by <a href="https://unsplash.com/@epicantus?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Daria Nepriakhina</a> on <a href="https://unsplash.com/t/business-work?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a> </p>
          </div>	
    </div>
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="/img/circuit.jpg" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <div class="banner-text">
              <h2>Circuit Test Management</h2>
              <p>Photo by Alexandre Debiève on Unsplash</p>
          </div>	
      </div>
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="/img/function1.jpg" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <div class="banner-text">
              <h2>Function Test Management</h2>
              <p>Photo by Luke Chesser on Unsplash</p>
          </div>	
      </div>
    </div>
    
            </div>	   
          
      </div>
    </div>
  </div>
  </section>
    <script src="/script/jquery3.4.1.js"></script>
    <script src="/script/popper.js"></script>
    <script src="/script/bootstrap431.js"></script>
  </body>
</html>
