<?php
session_start();
include('includes/header-1.php');
include('includes/navbar.php');
?>



<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h2 class="m-0 font-weight-bold text-primary">Daily
      
      </h2>
      
    </div>

    <div class="card-body">

      <div class="table-responsive">

        <?php
        include('connect.php');

        $sql = "SELECT * FROM DQCRD04";
        $query = mysqli_query($con, $sql);
        ?>


        <table class="table table-striped table-bordored mydatatable " id="dataTable" style="width: 100%" cellspacing="0">
          <thead>
          <tr>
                    <th colspan="1" rowspan="2">YYYYMMDD</th>
                   
                    
              </tr>
            <tr>
      
              <th> CUST</th>
              <th> MODEL </th>
              <th> MACHINE-ID </th>
              <th> GOOD </th>
              <th> NG </th>
              <th> Y/R </th>
              <th> Comment </th>
            </tr>
          </thead>
          <tbody>

            <?php
            if (mysqli_num_rows($query) > 0) {
              while ($row = mysqli_fetch_assoc($query)) {
                ?>
                <tr>
                  <td><?php echo $row['YYYYMMDD']; ?></td>
                  <td><?php echo $row['CUST']; ?></td>
                  <td><?php echo $row['MODEL']; ?></td>
                  <td><?php echo $row['MACHINE_ID']; ?></td>
                  <td><?php echo $row['GOOD_TOT_QTY']; ?></td>
                  <td><?php echo $row['BAD_TOT_QTY']; ?></td>
                  <td><?php echo $row['YIELD_RATE']; ?></td>

                  <td>
                    <form action="connect.php" method="post">
                      <input type="hidden" name="edit_id" value="<?php echo $row['YYYYMMDD']; ?>">
                      <button type="submit" name="comment_btn" class="btn btn-success"> Comment</button>
                    </form>
                  </td>

                </tr>
            <?php
              }
            } else {
              echo "No Record Found";
            }
            ?>
            <tr>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>


            </tr>

          </tbody>
        </table>

      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->





<?php
include('includes/footer.php');
include('includes/script-1.php');
?>