<?php 
session_start();
include('includes/header.php');
include('includes/navbar.php'); 
?>



<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> &times; </span>
        </button>
      </div>
      <form action="register-connect.php" method="POST">
        <input type="text" name="edit_id" value=" <?php echo $row['ID'] ?> ">
        <div class="modal-body">

            <div class="form-group">
                <label> Username </label>
                <input type="text" id="Username" name="Username" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" id="Password" name="Password" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label> Firstname </label>
                <input type="text" id="Firstname" name="Firstname" class="form-control" placeholder="">
            </div>
            <div class="form-group">
                <label> Lastname </label>
                <input type="text" id="Lastname" name="Lastname" class="form-control" placeholder="">
            </div>
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">User Accout 
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add User Accout 
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php 
    if(isset($_SESSION['success']) && $_SESSION['success'] != '') {
      echo '<h2 class="text-primary"> '.$_SESSION['success'].' </h2>';
      unset($_SESSION['success']);
    }

    if(isset($_SESSION['status']) && $_SESSION['status'] != '') {
      echo '<h2 class="text-danger"> '.$_SESSION['status'].' </h2>';
      unset($_SESSION['status']);
    }
    
    ?>

    <div class="table-responsive">

        <?php 
            include('connect.php');

            $sql = "SELECT * FROM user";
            $query = mysqli_query($con, $sql);
        ?>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> ID </th>
            <th> Username </th>
            <th> Password (Encrypt) </th>
            <th> Firstname </th>
            <th> Lastname </th>
            <th> Edit </th>
            <th> Delete </th>
          </tr>
        </thead>
        <tbody>

            <?php 
                if (mysqli_num_rows($query) > 0) {
                    while ($row = mysqli_fetch_assoc($query)) {
                        ?>
                    <tr>
                        <td><?php echo $row['ID']; ?></td>
                        <td><?php echo $row['Username']; ?></td>
                        <td><?php echo $row['Password']; ?></td>
                        <td><?php echo $row['Firstname']; ?></td>
                        <td><?php echo $row['Lastname']; ?></td>
                        <td>
                            <form action="register_edit.php" method="post">
                                <input type="hidden" name="edit_id" value="<?php echo $row['ID']; ?>">
                                <button  type="submit" name="edit_btn" class="btn btn-success"> EDIT</button>
                            </form>
                        </td>
                        <td>
                            <form action="delete-data.php" method="post">
                                <input type="hidden" name="delete_id" value=" <?php echo $row['ID']; ?>">
                                <button type="submit" name="delete_btn" class="btn btn-danger"> DELETE</button>
                            </form>
                        </td>
                        
                    </tr>
                    <?php 
                    }
                } else {
                    echo "No Record Found";
                }
            ?>
          <tr>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            
            
          </tr>
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->


<?php
include('includes/footer.php');
include('includes/script.php');
?>