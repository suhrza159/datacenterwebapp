<?php 
session_start();
header('Content-Type: application/json');
include('connect.php');

    
$sumWeekData = sprintf("SELECT WEEK, CUST, MODEL, MACHINE_ID, SUM(GOOD_TOT_QTY) AS GOODTOT, SUM(NG_TOT_QTY) AS NGTOT, (SUM(GOOD_TOT_QTY) / (SUM(GOOD_TOT_QTY) + SUM(NG_TOT_QTY)) * 100) AS RATE, FYR AS YR FROM WEEKLY GROUP BY WEEK, CUST, MODEL, MACHINE_ID");
$result1 = $con -> query($sumWeekData);

$weekData = array();
foreach ($result1 as $row) {
   $weekData[] = $row;
}

$result1 -> close();
$con -> close();
print json_encode($weekData);
?>

        