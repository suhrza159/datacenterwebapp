<?php
session_start();
include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container">
    <div class="row my-3">
        <div class="col">
            <h4>Daily Chart</h4>
        </div>
    </div>
    

    <div class="row my-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <canvas id="myChart" height="100"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/script/charts.js"></script>









<?php
include('includes/script.php');
include('includes/footer.php');
?>