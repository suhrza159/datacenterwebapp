 <!-- Bootstrap core JavaScript-->
 <script src="/vendor/jquery/jquery.min.js"></script>
 <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Core plugin JavaScript-->
 <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

 <!-- Custom scripts for all pages-->
 <script src="/js/sb-admin-2.min.js"></script>

 <!-- Page level plugins -->
 <script src="/vendor/chart.js/Chart.min.js"></script>
 <script src="/script/datatable1109.js"></script>
 <script src="/script/datatable-bootstrap1-10-9.js"></script>
 <script>
   $('.mydatatable').DataTable({
     searching: true,
     ordering: false,
     lengthMenu: [
       [5, 10, 25, 50, -1],
       [5, 10, 25, 50, "All"]
     ],
     createdRow: function(row, data, index) {
       if (data[5].replace(/[\$,]/g, '') * 1 > 150000) {
         $('td', row).eq(5).addClass('text-success');
       }
     }
   });
 </script>

 <!-- Page level custom scripts -->
 <script src="/js/demo/chart-area-demo.js"></script>
 <script src="/js/demo/chart-pie-demo.js"></script>