<?php 
session_start();
header('Content-Type: application/json');
include('connect.php');

$sumGoodAndNGFromDay = sprintf("SELECT (SUM(GOOD_TOT_QTY) / (SUM(GOOD_TOT_QTY) + SUM(BAD_TOT_QTY)) * 100) AS YR, CAST(YYYYMMDD AS DATE) AS YYYYMMDD, CAST(SUM(GOOD_TOT_QTY + BAD_TOT_QTY) AS SIGNED) AS TTLQTY FROM DQCRD04 GROUP BY YYYYMMDD");
$result1 = $con -> query($sumGoodAndNGFromDay);

$data = array();
foreach ($result1 as $row) {
   $data[] = $row;
}


$result1 -> close();
$con -> close();
print json_encode($data);


?>