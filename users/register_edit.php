<?php 
session_start();
include('includes/header.php');
include('includes/navbar.php');
?>
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> Edit User Profile 
        
    </h6>
  </div>

  <div class="card-body">
  <?php 
    if(isset($_SESSION['success']) && $_SESSION['success'] != '') {
      echo '<h2> '.$_SESSION['success'].' </h2>';
      unset($_SESSION['success']);
    }

    if(isset($_SESSION['status']) && $_SESSION['status'] != '') {
      echo '<h2 class="bg-info"> '.$_SESSION['status'].' </h2>';
      unset($_SESSION['status']);
    }
    
    ?>

    <?php
    include('connect.php');
    if(isset($_POST['edit_btn'])) {
        
        $id = $_POST['edit_id'];
        
        $sql = "SELECT * FROM user WHERE id='$id' ";
    
        $query1 = mysqli_query($con, $sql);

        foreach ($query1 as $row) {
            
            ?>
        <form action="edit-user.php" method="POST">
        <input type="hidden" name="edit_id" value="<?php echo $row['ID']; ?>">
        <div class="form-group">
            <label> Username </label>
            <input type="text" name="edit_Username" value="<?php echo $row['Username'] ?>"  class="form-control" placeholder="">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="edit_Password" value="<?php echo $row['Password'] ?>"  class="form-control" placeholder="">
        </div>
        <div class="form-group">
            <label> Firstname </label>
            <input type="text" name="edit_Firstname" value="<?php echo $row['Firstname'] ?>"  class="form-control" placeholder="">
        </div>
        <div class="form-group">
            <label> Lastname </label>
            <input type="text" name="edit_Lastname" value="<?php echo $row['Lastname'] ?>"  class="form-control" placeholder="">
        </div>
        <button type="submit"  name="updatebtn" class="btn btn-primary"> Update </button>
        <a href="delete-user.php" class="btn btn-danger"> Cancel</a>
        
        </form>
        

        <?php }
    } ?>
  </div>
</div>

</div>
<!-- /.container-fluid -->


<?php 
include('includes/footer.php');
include('includes/script.php');
?>