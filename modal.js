const modal = document.querySelector('#myModal');
const modalBtn = document.querySelector('#modalBtn');
const closeBtn = document.querySelectorAll('#fas fa-times-circle');

modalBtn.addEventListener('click', () => {
    modal.style.display = "block";
});

closeBtn.addEventListener('click', () => {
    modal.style.display = "none";
});

window.addEventListener('click', (event) => {
    if(event.target === modal) return;
});