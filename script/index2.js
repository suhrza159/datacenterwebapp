$(function() {
    $('.hamburger-menu').on('click', function() {
        $('.toggle').toggleClass('open');
        $('.nav-list').toggleClass('open');
        $('toggle').fadeIn();
    });

    AOS.init({
        easing: 'ease',
        duration: 1000,
    })
});

$("#signup").click(function() {
    $("#first").fadeOut("fast", function() {
    $("#second").fadeIn("fast");
    });
    });
    
    $("#signin").click(function() {
    $("#second").fadeOut("fast", function() {
    $("#first").fadeIn("fast");
    });
    });

$(function() {
    $("form[name='login']").validate({
      rules: {
        
        userid: {
          required: true
        },
        password: {
          required: true,
          
        }
      },
       messages: {
        userid: "Please enter a valid userid    ",
       
        password: {
          required: "Please enter password",
         
        }
        
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  });