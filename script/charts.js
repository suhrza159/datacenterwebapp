$(document).ready(function() {
    $.ajax({
        url: "http://localhost/users/data-for-chart.php",
        method: "GET",
        success: function(data) {
            console.log(data);
            var day = [];
            var total = [];
            var yr = [];

            for(var i in data) {
                day.push(data[i].YYYYMMDD);
                total.push(data[i].TTLQTY);
                yr.push(data[i].YR);
            }

            var ctx = $("#myChart");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: day,
                    datasets: [{
                        type: 'line',
                        label: 'RATE',
                        fill: false,
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: yr,
                        yAxisID: 'right_axis'
                    },{
                        type: 'bar',
                        label: 'TTL-QTY',
                        fill: true,
                        borderColor: 'rgba(75, 192, 192, 1)',
                        backgroundColor: 'rgba(75, 192, 192, 1)',
                        data: total,
                        yAxisID: 'left_axis'
                    }]
                },
                options: {
                    legend: {position:'top', usePointStyle:true},
                    
                    responsive: true,
                    title: {display: false},
                    tooltips: {mode: 'index', intersect: false},
                    hover: {mode: 'nearest', intersect: true},
                    scales: {
                        xAxes: [{display: true, stacked:true, scaleLabel: {display: false, labelString: 'time'}}],
                        yAxes: [{
                            type:'linear',
                            id:'right_axis',
                            display: true,
                            position: 'right',
                            scaleLabel: {display: true, labelString: '%'},
                            gridLines: {drawOnChartArea:false},
                            stacked:false,
                            
                        },{
                            type:'linear',
                            id:'left_axis',
                            display: true,
                            position: 'left',
                            
    
                            
                        }]
                    },
                }
            })
        }
    });
});